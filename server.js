const parseXlsx = require('excel').default;
require('dotenv').config()
const express = require("express");
const app = express();
const redis = require("redis");
const ENVIRONMENT = "coalesce"
const client = ENVIRONMENT != "qovery" ? redis.createClient() : redis.createClient(process.env.QOVERY_DATABASE_MY_REDIS_CONNECTION_URI_WITHOUT_CREDENTIALS)
const cron = require('node-cron');
const fetch = require('node-fetch');
const DEPLOYED_EXCEL_PATH = "deployment_sheet.xlsx"
// const  bodyParser = require('body-parser')
const router = express.Router();
app.use(express.json({ limit: "100mb" }));
// app.use(express.compress());
// app.use(bodyParser.json({ extended: true, limit: "100mb" }));
// Create a single supabase client for interacting with your database 


const key_names = [
  'verified',
  'not_available',
  'need_verification',
  'discarded',
  'busy',
  'headers',
  'keys_name'
]
const keyNameToRejectWhileParsing = ["headers", "keys_name"]

const options = [
  {
    "value": "hospital_beds",
    "label": "Hospital Beds"
  },
  {
    "value": "plasma_donors",
    "label": "Plasma Donors"
  },
  {
    "value": "oxygen_list",
    "label": "Oxygen List"
  },
  {
    "value": "medicine_list",
    "label": "Medicine List"
  },
  {
    "value": "food_list",
    "label": "Food List"
  },
  {
    "value": "home_nurse_list",
    "label": "Home Nurse List"
  },
  {
    "value": "ambulance_list",
    "label": "Ambulance List"
  },
  {
    "value": "whatsapp_telegram_group",
    "label": "Whatsapp Telegram Group"
  }
]






const segregateResponseJsonIntokeynames = (JsonResponseFromGoogleSheet) => {


  let retJsonOfArrayofJson = key_names.filter(x => keyNameToRejectWhileParsing.indexOf(x) < 0).reduce((acc, item) => ({ ...acc, [item]: [] }), {})
  //means jsonwithkeynames initialized as empty arry
  Object.keys(JsonResponseFromGoogleSheet).forEach(item => {
    const headers = JsonResponseFromGoogleSheet[item]["headers"]
    key_names.filter(x => keyNameToRejectWhileParsing.indexOf(x) < 0)
      .forEach(item1 => {
        console.log(item1, item, "Y")
        const fullKeyArrayOfArr = JsonResponseFromGoogleSheet[item][item1].map(dataArr => {
          return dataArr.reduce((acc, dataItem, dataItemIndex) => {
            return { ...acc, [headers[dataItemIndex]]: dataItem }
          }, { "sub_sheet_name": item })
        })
        retJsonOfArrayofJson[item1] = [...retJsonOfArrayofJson[item1], ...fullKeyArrayOfArr]

      })

  })
  return retJsonOfArrayofJson
}



const convertExcelArrayToArrayofJson = (headerArr, fulldataArrofArr) => {
  return fulldataArrofArr.slice(1).map((item, index) => {
    return item.reduce((acc, item1, index1) => {
      return { ...acc, [headerArr[index1]]: item1 }
    }, {})
  })
}

const returnedParsedExcelPromised = (fullExcelPath) => {
  return new Promise((resolve, reject) => {
    parseXlsx(fullExcelPath).then((data) => {
      // data is an array of arrays
      resolve(data)

    })
      .catch(err => {
        reject(err)
      })
  })

}
const setOneintoRedisPromised = (keyName, JsonObj, argName) => {

  console.log(argName, "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY")
  console.log(JsonObj)
  const argVal = options.find(x => x.label === JsonObj.resource_type).value
  console.log(argVal)
  return new Promise((resolve, reject) => {
    try {
      client.hset(keyName, argVal, JSON.stringify(JsonObj), (err, data) => {
        if (!err) resolve(data)
        else {
          reject(err)
        }
      })

    } catch (error) {
      reject(error)
    }

  })

}
const setAllIntoRedisArrayOfJson = (keyName, ArrayOfJson, argName) => {
  const promisedArr = ArrayOfJson.map(x => setOneintoRedisPromised(keyName, x, argName))
  return new Promise((resolve, reject) => {
    Promise.all(promisedArr)
      .then(res => resolve(res)).catch(err => reject(err))
  })

}

const promisedGetForResourceType = (arg_name, resource_type) => {
  return new Promise((resolve, reject) => {
    client.hget("all_data", `${arg_name}__${resource_type}`, (err, data) => {
      if (!err) resolve(data)
      else {
        reject(err)
      }
    })
  })

}


const getOneFromRedisPromisedWithMethodNameAsArg = (argName, resourceType) => {

  console.log(argName, "YYYY", resourceType)
  return new Promise((resolve, reject) => {
    try {
      if (resourceType !== "all") {

        client.hget("all_data", `${argName}__${resourceType}`, (err, data) => {
          if (!err) resolve(data)
          else {
            reject(err)
          }
        })
      } else {
        client.hgetall("all_data", (err, data) => {
          if (!err) {

            let getALlJson = data

            let returnJson = {}
            const key_names = [
              'verified',
              'not_available',
              'need_verification',
              'discarded',
              'busy'
            ]
            const allPromised = key_names.map(x => promisedGetForResourceType(argName, x))
            Promise.all(allPromised)
              .then(response1 => {
                console.log(response1, "PPPPPPPPPPPPPPRRRRRRRRRRRRRROOOOOOOOOOOMMMMMMMMMMMSSSSSIIIIEDDDDDDDDDDD")
              }).catch(err1 => {
                console.log(err1)
              })
          }
          else {
            reject(err)
          }
        })
      }

    } catch (error) {
      reject(error)
    }

  })

}

const setOneFromRedisPromisedWithExpiry15 = (argName, valueAsJson, expireySec = 17 * 60) => {
  return new Promise((resolve, reject) => {
    try {

      client.setex(argName, expireySec, JSON.stringify(valueAsJson), (err, data) => {
        if (!err) resolve(data)
        else {
          reject(err)
        }
      })

    } catch (error) {
      reject(error)
    }

  })

}

const fetchFromDeployedUrlAsJsonFromObj = (deployedUrl) => {
  return new Promise((resolve, reject) => {
    fetch(deployedUrl)
      .then(res1 => res1.json())
      .then(res => {

        resolve(res)
      })
      .catch(err => {
        console.log("errInUrl", deployedUrl)
        resolve({})

      })
  })

}

const setinRedisHSETfromobj = (obj, method_val, status) => {

  return new Promise((resolve, reject) => {
    client.hset("all_data", `${method_val}__${status}`, JSON.stringify(obj), (err, data) => {
      if (!err) {
        resolve(data)
      } else {
        reject(err)
      }
    })
  })



}
const promisedHsetWithHsetKeyAsMameArgAssheetUrlAndResponseAsValue = (dataObj, nameSheet, sheet_url, deployedUrl) => {

  return new Promise(async (resolve, reject) => {

    const argVal = options.find(x => x.label === dataObj.resource_type).value
    try {
      console.log("promisedHsetWithHsetKeyAsMameArgAssheetUrlAndResponseAsValue", { dataObj, nameSheet, sheet_url, deployedUrl, argVal })

      const fetchedVal = await fetchFromDeployedUrlAsJsonFromObj(deployedUrl)
      console.log(fetchedVal, "+++++++++++++++++++++++++++++++++++", deployedUrl)
      if (Object.keys(fetchedVal).length > 0) {

        const key_names1 = [
          'verified',
          'not_available',
          'need_verification',
          'discarded',
          'busy',

        ]
        const allValuesArr = key_names1.map(x => setinRedisHSETfromobj({ "sheet_url": sheet_url, "updatedAt": Date.now(), data: fetchedVal[x] }, argVal, x))

        Promise.all(allValuesArr)
          .then(response12 => {
            console.log(response12)
          }).catch(error1 => console.log(error1))

        resolve(1)
      } else {

        console.log("not set", nameSheet)


        resolve(1)
      }


    } catch (error) {
      reject(error)
    }
  })



}

const getFromRedisHitAPiAndSetInRedisInSeparateKeysHset = (dataObjStr) => {

  console.log("getFromRedisHitAPiAndSetInRedisInSeparateKeysHset", dataObjStr)
  const dataObj = JSON.parse(dataObjStr)
  const arg_name = dataObj["resource_type"]
  const sheet_name = dataObj["sheet_name"]
  const sheet_url = dataObj["sheet_url"]
  const deployedUrl = dataObj["deployed_url"]
  return promisedHsetWithHsetKeyAsMameArgAssheetUrlAndResponseAsValue(dataObj, sheet_name, sheet_url, deployedUrl)

}

cron.schedule('*/10 * * * *', () => {
  console.log("cron ran")
  client.hgetall("sheet_data", (err, data) => {
    if (!err) {
      console.log(data, "DDDDDDDDDDDDDDDDDDDDDDDDDDD")
      const allValues = Object.keys(data);

      const promisedArrOfAll = allValues.map(x => getFromRedisHitAPiAndSetInRedisInSeparateKeysHset(data[x]))

      Promise.all(promisedArrOfAll)
        .then(response => {
          console.log("latest data set into redis at", new Date().toLocaleString())

        }).catch(error => {
          console.log(error)

        })

    }
    else {

      console.log(err)

    }
  });

});

const sendDelayedResponsewithLagechCrawledData = (method_name, resourceType) => {
  return new Promise((resolve, reject) => {
    getOneFromRedisPromisedWithMethodNameAsArg(method_name, resourceType)
      .then(resp => {
        resolve(resp)
      }).catch(err1 => {

        console.log(err1)
        reject(err1)
      }


      )
  })



}
// https://covid-available-resource.vercel.app/
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.post("/api", (request, response, next) => {
  // express helps us take JS objects and send them as JSON
  const body = request.body

  console.log(body, "YYY")
  const method_name = body["method_name"]
  const token = body["dev_token"]
  const resource_type = body["resource_type"]
  console.log(method_name, token, resource_type)


  switch (method_name) {
    case "set_sheet_data_c":
      returnedParsedExcelPromised(DEPLOYED_EXCEL_PATH)
        .then(res => {
          // console.log(res, "response is")
          const leavEmpty = res.map(item1 => {
            return item1.filter(z => !!z)
          }).filter(q => q.length > 0)
          console.log(leavEmpty, "REEEEEEEEEEEEEEEE")
          const data = leavEmpty
          const headers = data[0]
          const fullData = data
          console.log(data.length, "UUUU", headers.length)
          const fullArrofJson = convertExcelArrayToArrayofJson(headers, fullData)
          // console.log(fullArrofJson)
          return setAllIntoRedisArrayOfJson("sheet_data", fullArrofJson, resource_type)
        })
        .then(res1 => {
          // console.log("response of supabase insertion", res1)
          response.status(200).json({ "msg": "SHEET_DATA_SET_IN_REDIS" });
        })
        .catch(err => {
          console.log(err)

          response.status(500).json({ "msg": "some erroe ocurred" })
        })
      break;
    case "updated_cron_manuallY_c":
      console.log("cron ran")
      client.hgetall("sheet_data", (err, data) => {
        if (!err) {
          console.log(data, "DDDDDDDDDDDDDDDDDDDDDDDDDDD")
          const allValues = Object.keys(data);

          const promisedArrOfAll = allValues.map(x => getFromRedisHitAPiAndSetInRedisInSeparateKeysHset(data[x]))

          Promise.all(promisedArrOfAll)
            .then(response => {
              console.log("latest data set into redis at", new Date().toLocaleString())
              response.json({ "msg": "DATA_FETCHED_AND_SET" });
            }).catch(error => {
              console.log(error)
              response.json({ "msg": "some erroe ocurred" })
            })

        }
        else {

          console.log(err)
          response.json({ "msg": "some erroe ocurred" })
        }
      });
      break;
    default:
      getOneFromRedisPromisedWithMethodNameAsArg(method_name, resource_type)
        .then(response1 => {
          console.log("YAHHHAAA AAYA", response1)
          if (!response1) {
            returnedParsedExcelPromised(DEPLOYED_EXCEL_PATH)
              .then(res => {
                // console.log(res, "response is")
                const leavEmpty = res.map(item1 => {
                  return item1.filter(z => !!z)
                }).filter(q => q.length > 0)
                console.log(leavEmpty, "REEEEEEEEEEEEEEEE")
                const data = leavEmpty
                const headers = data[0]
                const fullData = data
                console.log(data.length, "UUUU", headers.length)
                const fullArrofJson = convertExcelArrayToArrayofJson(headers, fullData)
                // console.log(fullArrofJson)
                return setAllIntoRedisArrayOfJson("sheet_data", fullArrofJson, resource_type)
              }).then(res1 => {
                console.log("data is reset", res1)
                sendDelayedResponsewithLagechCrawledData(method_name, resource_type)
                  .then(resp123 => {

                    response.status(200).json(JSON.parse(resp123))
                  })
                  .catch(err => console.log(err))


              })
              .catch(err1 => console.log("ERRRRRRRRRRR", err1))
          } else {
            console.log(response1, "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYGH")
            // response.send().json()
            response.status(200).json(JSON.parse(response1))
          }


        })
        .catch(err => {
          console.log(err)
          response.status(500).json({ "msg": "some erroe ocurred" })

        })


  }




});
app.use('/', router);
// listen for requests :)
const listener = app.listen(9000, () => {
  console.log("Your app is listening on port " + listener.address().port);
});