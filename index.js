const parseXlsx = require('excel').default;
require('dotenv').config()
const express = require("express");
const app = express();
const redis = require("redis");
const ENVIRONMENT = "coalesce"
const client = ENVIRONMENT != "qovery" ? redis.createClient() : redis.createClient(process.env.QOVERY_DATABASE_MY_REDIS_CONNECTION_URI_WITHOUT_CREDENTIALS)
const cron = require('node-cron');
const fetch = require('node-fetch');
const DEPLOYED_EXCEL_PATH = "deployed_url_sheet.xlsx"
// const  bodyParser = require('body-parser')
const router = express.Router();
app.use(express.json({ limit: "100mb" }));
// app.use(express.compress());
// app.use(bodyParser.json({ extended: true, limit: "100mb" }));
// Create a single supabase client for interacting with your database 


const DEPLOYMENT_URL = "https://script.google.com/macros/s/AKfycbwof1JY2HC9sHNw-hUU1zgZLTJkZz9kl9hZSDRZU5Llu4lmulyIZJ143zRapB3rhXdcdw/exec"

const key_names = [
  'verified',
  'not_available',
  'need_verification',
  'discarded',
  'busy',
  'headers',
  'keys_name'
]
const keyNameToRejectWhileParsing = ["headers", "keys_name"]

const options = [
  {
    "value": "hospital_beds",
    "label": "Hospital Beds"
  },
  {
    "value": "plasma_donors",
    "label": "Plasma Donors"
  },
  {
    "value": "oxygen_list",
    "label": "Oxygen List"
  },
  {
    "value": "medicine_list",
    "label": "Medicine List"
  },
  {
    "value": "food_list",
    "label": "Food List"
  },
  {
    "value": "home_nurse_list",
    "label": "Home Nurse List"
  },
  {
    "value": "testing_list",
    "label": "Testing List"
  },
  {
    "value": "ambulance_list",
    "label": "Ambulance List"
  },
  {
    "value": "doctor_list",
    "label": "Doctor List"
  },
  {
    "value": "menta_health_list",
    "label": "Menta Health List"
  },
  {
    "value": "whatsapp_telegram_group",
    "label": "Whatsapp Telegram Group"
  },
  {
    "value": "oxygen_list_delhi",
    "label": "Oxygen List Delhi"
  }
]






const segregateResponseJsonIntokeynames = (JsonResponseFromGoogleSheet) => {


  let retJsonOfArrayofJson = key_names.filter(x => keyNameToRejectWhileParsing.indexOf(x) < 0).reduce((acc, item) => ({ ...acc, [item]: [] }), {})
  //means jsonwithkeynames initialized as empty arry
  Object.keys(JsonResponseFromGoogleSheet).forEach(item => {
    const headers = JsonResponseFromGoogleSheet[item]["headers"]
    key_names.filter(x => keyNameToRejectWhileParsing.indexOf(x) < 0)
      .forEach(item1 => {
        console.log(item1, item, "Y")
        const fullKeyArrayOfArr = JsonResponseFromGoogleSheet[item][item1].map(dataArr => {
          return dataArr.reduce((acc, dataItem, dataItemIndex) => {
            return { ...acc, [headers[dataItemIndex]]: dataItem }
          }, { "sub_sheet_name": item })
        })
        retJsonOfArrayofJson[item1] = [...retJsonOfArrayofJson[item1], ...fullKeyArrayOfArr]

      })

  })
  return retJsonOfArrayofJson
}



const convertExcelArrayToArrayofJson = (headerArr, fulldataArrofArr) => {
  return fulldataArrofArr.map((item, index) => {
    return item.reduce((acc, item1, index1) => {
      return { ...acc, [headerArr[index1]]: item1 }
    }, {})
  })
}

// const returnedParsedExcelPromised = (fullExcelPath) => {
//   return new Promise((resolve, reject) => {
//     parseXlsx(fullExcelPath).then((data) => {
//       // data is an array of arrays
//       resolve(data)

//     })
//       .catch(err => {
//         reject(err)
//       })
//   })

// }
const returnedParsedExcelPromised = (fullExcelPath) => {
  return new Promise((resolve, reject) => {
    parseXlsx(fullExcelPath).then((data) => {
      // data is an array of arrays
      resolve(data)

    })
      .catch(err => {
        reject(err)
      })
  })

}

const setOneintoRedisPromised = (keyName, JsonObj, argName) => {
  return new Promise((resolve, reject) => {
    try {
      client.hset(keyName, JsonObj[argName], JSON.stringify(JsonObj), (err, data) => {
        if (!err) resolve(data)
        else {
          reject(err)
        }
      })

    } catch (error) {
      reject(error)
    }

  })

}
const setAllIntoRedisArrayOfJson = (keyName, ArrayOfJson, argName) => {
  const promisedArr = ArrayOfJson.map(x => setOneintoRedisPromised(keyName, x, argName))
  return new Promise((resolve, reject) => {
    Promise.all(promisedArr)
      .then(res => resolve(res)).catch(err => reject(err))
  })

}

// returnedParsedExcelPromised(DEPLOYED_EXCEL_PATH)
//   .then(res => {
//     // console.log(res, "response is")

//     const data = res
//     const headers = data[0]
//     const fullData = data
//     const fullArrofJson = convertExcelArrayToArrayofJson(headers, fullData)
//     // console.log(fullArrofJson)
//     return setAllIntoRedisArrayOfJson("sheet_data", fullArrofJson, "name")
//   })
//   .then(res1 => {
//     console.log("response of supabase insertion", res1)
//   })
//   .catch(err => { console.log(err) })


const getOneFromRedisPromisedWithMethodNameAsArg = (argName) => {
  return new Promise((resolve, reject) => {
    try {

      client.get(argName, (err, data) => {
        if (!err) resolve(data)
        else {
          reject(err)
        }
      })

    } catch (error) {
      reject(error)
    }

  })

}

const setOneFromRedisPromisedWithExpiry15 = (argName, valueAsJson, expireySec = 17 * 60) => {
  return new Promise((resolve, reject) => {
    try {

      client.setex(argName, expireySec, JSON.stringify(valueAsJson), (err, data) => {
        if (!err) resolve(data)
        else {
          reject(err)
        }
      })

    } catch (error) {
      reject(error)
    }

  })

}

const HSETONEPromised = (key, argName, value) => {
  return new Promise((resolve, reject) => {
    try {
      client.HSET(`${key}__${argName}`, argName, JSON.stringify(value), (err, data) => {
        if (!err) resolve(data)
        else {
          reject(err)
        }
      })
    } catch (error) {
      reject(error)
    }

  })

}
const setOneFromRedisPromised = (argName, valueAsJson) => {
  return new Promise((resolve, reject) => {

    const promisedUpdates = key_names.filter(x => !keyNameToRejectWhileParsing(x)).map(y => HSETONEPromised(y, argName, valueAsJson))
    Promise.all(promisedUpdates)
      .then(response => {
        consolelog("all set in hset")
      }).catch(err => console.log("error setting hset for", argName))

  })

}
const fetchFromDeployedUrlAsJsonFromObj = (deployedUrl) => {
  return new Promise((resolve, reject) => {
    fetch(deployedUrl)
      .then(res1 => res1.json())
      .then(res => {

        resolve(res)
      })
      .catch(err => {
        console.log("errInUrl", deployedUrl)
        resolve({})

      })
  })

}
const promisedHsetWithHsetKeyAsMameArgAssheetUrlAndResponseAsValue = (nameSheet, sheet_url, deployedUrl) => {

  return new Promise(async (resolve, reject) => {

    try {
      const fetchedVal = await fetchFromDeployedUrlAsJsonFromObj(deployedUrl)

      if (Object.keys(fetchedVal).length > 0) {

        // setSegregatedInto
        const valueToSetInTTl = { "sheet_url": sheet_url, "updatedAt": Date.now(), ...fetchedVal }

        const setInRedis = await setOneFromRedisPromised(nameSheet, valueToSetInTTl)

        resolve(1)
      } else {

        console.log("not set", nameSheet)
        // const valueToSetInTTl = { "sheet_url": sheet_url, ...segregateResponseJsonIntokeynames(fetchedVal) }

        // const setInRedis = await setOneFromRedisPromisedWithExpiry15(nameSheet, valueToSetInTTl)

        resolve(1)
      }


    } catch (error) {
      reject(error)
    }
  })



}

const getFromRedisHitAPiAndSetInRedisInSeparateKeysHset = (dataObjStr) => {
  const dataObj = JSON.parse(dataObjStr)
  const arg_name = dataObj["resource_type"]
  const sheet_url = dataObj["sheet_url"]
  const deployedUrl = dataObj["deployed url"]
  return promisedHsetWithHsetKeyAsMameArgAssheetUrlAndResponseAsValue(arg_name, sheet_url, deployedUrl)

}

cron.schedule('*/10 * * * *', () => {
  console.log("cron ran")
  client.hgetall("sheet_data", (err, data) => {
    if (!err) {

      const allValues = Object.keys(data);

      const promisedArrOfAll = allValues.map(x => getFromRedisHitAPiAndSetInRedisInSeparateKeysHset(data[x]))

      Promise.all(promisedArrOfAll)
        .then(response => {
          console.log("latest data set into redis at", new Date().toLocaleString())
        }).catch(error => {
          console.log(error)
        })

    }
    else { console.log(err) }
  });

});
// https://covid-available-resource.vercel.app/
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.post("/api", (request, response, next) => {
  // express helps us take JS objects and send them as JSON
  const body = request.body

  console.log(body, "YYY")
  const method_name = body["method_name"]
  const token = body["dev_token"]
  console.log(method_name, token)
  if (token == process.env.DEV_SECRET) {

    switch (method_name) {
      case "set_sheet_data_c":
        // fetchFromDeployedUrlAsJsonFromObj(DEPLOYMENT_URL)
        returnedParsedExcelPromised(DEPLOYED_EXCEL_PATH)
          .then(res => {
            // console.log(res, "response is")

            const data = res
            const headers = res.headers
            const fullData = res.deployed
            const fullArrofJson = convertExcelArrayToArrayofJson(headers, fullData)
            // console.log(fullArrofJson)
            return setAllIntoRedisArrayOfJson("sheet_data", fullArrofJson, "resource_type")
          })
          .then(res1 => {
            // console.log("response of supabase insertion", res1)
            response.json({ "msg": "SHEET_DATA_SET_IN_REDIS" });
          })
          .catch(err => {
            console.log(err)

            response.json({ "msg": "some erroe ocurred" })
          })
        break;
      case "updated_cron_manuallY_c":
        client.hgetall("sheet_data", (err, data) => {
          if (!err) {

            const allValues = Object.keys(data);

            const promisedArrOfAll = allValues.map(x => getFromRedisHitAPiAndSetInRedisInSeparateKeysHset(data[x]))

            Promise.all(promisedArrOfAll)
              .then(response => {
                console.log("latest data set into redis at", new Date().toLocaleString())
                response.json({ "msg": "DATA_FETCHED_AND_SET" });
              }).catch(error => {
                console.log(error)
                response.json({ "msg": "some erroe ocurred" })
              })

          }
          else {

            console.log(err)
            response.json({ "msg": "some erroe ocurred" })
          }
        });
        break;
      default:
        getOneFromRedisPromisedWithMethodNameAsArg(method_name)
          .then(response1 => {
            if (!response1) {
              client.hgetall("sheet_data", (err, data) => {
                if (!err) {

                  const allValues = Object.keys(data);

                  const promisedArrOfAll = allValues.map(x => getFromRedisHitAPiAndSetInRedisInSeparateKeysHset(data[x]))

                  Promise.all(promisedArrOfAll)
                    .then(response => {
                      console.log("latest data set into redis at", new Date().toLocaleString())

                    }).catch(error => {
                      console.log(error)

                    })

                }
                else {

                  console.log(err)

                }
              });
            }
            response.json(JSON.parse(response1))
          })
          .catch(err => {
            response.json({ "msg": "some erroe ocurred" });
          })


    }
    // if (method_name == "set_sheet_data_c") {
    //   returnedParsedExcelPromised(DEPLOYED_EXCEL_PATH)
    //     .then(res => {
    //       // console.log(res, "response is")

    //       const data = res
    //       const headers = data[0]
    //       const fullData = data
    //       const fullArrofJson = convertExcelArrayToArrayofJson(headers, fullData)
    //       // console.log(fullArrofJson)
    //       return setAllIntoRedisArrayOfJson("sheet_data", fullArrofJson, "name")
    //     })
    //     .then(res1 => {
    //       // console.log("response of supabase insertion", res1)
    //       response.json({ "msg": "SHEET_DATA_SET_IN_REDIS" });
    //     })
    //     .catch(err => {
    //       console.log(err)
    //       response.json({ "msg": "some erroe ocurred" })
    //     })
    // } else {
    //   getOneFromRedisPromisedWithMethodNameAsArg(method_name)
    //     .then(response1 => {
    //       response.json(JSON.parse(response1))
    //     })
    //     .catch(err => {
    //       response.json({ "msg": "some erroe ocurred" });
    //     })
    // }




  } else {
    response.json({ "msg": "some erroe ocurred" });
  }
});
app.use('/', router);
// listen for requests :)
const listener = app.listen(ENVIRONMENT != "qovery" ? process.env.PORT : process.env.MY_NODE_PORT, () => {
  console.log("Your app is listening on port " + listener.address().port);
});