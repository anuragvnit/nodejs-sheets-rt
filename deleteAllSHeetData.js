const keyName = "sheet_data"
const redis = require("redis");
const ENVIRONMENT = "coalesce"
const client = ENVIRONMENT != "qovery" ? redis.createClient() : redis.createClient(process.env.QOVERY_DATABASE_MY_REDIS_CONNECTION_URI_WITHOUT_CREDENTIALS)


const deleteOneArgPromised = (keyName1, argName) => {
  return new Promise((resolve, reject) => {
    client.hdel(keyName1, argName, (err, data) => {
      if (!err) {
        resolve(1)
      } else {
        reject(1)
      }
    })
  })

}

const deleteBykeyName = (keyName1 = keyName) => {
  client.hgetall(keyName1, (err, data) => {
    if (err) {
      console.log("ERRRRIR 1")
    } else {
      const dataKeys = Object.keys(data)
      const allPromised = dataKeys.map(y => deleteOneArgPromised(keyName1, y))
      Promise.all(allPromised)
        .then(res => console.log(res, "all deleted"))
        .catch(err => console.log("error deleting"))
    }
  })
}

deleteBykeyName(keyName)