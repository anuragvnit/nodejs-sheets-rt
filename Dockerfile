FROM node:12.9.1

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .
RUN npm install
EXPOSE 7777
CMD [ "node", "index.js" ]
